i3-autoname
===========
> Change workspace name based on the running apps

![Sample Gif](https://gitlab.com/aquator/i3-autoname/-/raw/master/sample.gif)

Features
--------
- Automatically adjusts workspace names based on applications running on them
- Customizable icons (or text) based on `WM_CLASS` and `WM_INSTANCE`
- Xresources based configuration (with the built-in fallback)
- Optional icon limit
- Optional unique filter for show single icon even if multiple apps are running

Usage
-----
>Note: this contains unicode characters your browser may not show properly.
```
Usage: i3-autoname [-h] [-d] [-u] [-m <number>]
Automatically renames i3wm workspaces to pre-configured icons based on running applications.
It expects and uses number:name workspace names format, so all your shortcuts can work.

Use .Xresources to configure wsIcon property to the string you want to be displayed for
an application, based on WM_CLASS and WM_INSTANCE.

Examples:
*.wsIcon: []
URxvt*wsIcon: 
Sublime_text*wsIcon: 

Default (empty) workspace names can be set in .Xresources too:
i3wm.ws1.name: 1:
i3wm.ws2.name: 2:

Arguments:
 -h            Prints this help page.
 -u            Only shows an icon once, even for multiple instances.
 -m <number>   Maximum number of icons displayed.
```

Requirements
------------
i3-autoname is written in [Bash] for [i3wm], and using [jq] to parse responses from `i3-msg`. It also uses some generic tools like `head`, `sed`, `sort`, `tr`, `xargs` and `xgetres`.

For best visuals it is recommended to use [Polybar] with [i3 module], and a nice font icon, like [Font Awesome] or [Nerd Fonts].

Examples
--------
>Note: this contains unicode characters your browser may not show properly.

### .Xresources
```
! Default Workspace Names
! Use number:name format as value
i3wm.ws0.name: 0: 
i3wm.ws1.name: 1:
i3wm.ws2.name: 2:
i3wm.ws3.name: 3:
i3wm.ws4.name: 4:
i3wm.ws5.name: 5:
i3wm.ws6.name: 6:
i3wm.ws7.name: 7:
i3wm.ws8.name: 8:
i3wm.ws9.name: 9: 

! Workspace Icons for Apps
! Use <class>.<instance>.wsIcon as key
! An empty value will not show the icon.
*.wsIcon: 
URxvt*wsIcon: 
Evolution*wsIcon: 
Brave-browser*wsIcon: 
jetbrains-idea-ce*wsIcon: 
Microsoft Teams - Preview*wsIcon: 
Sublime_text*wsIcon: 
Sublime_merge*wsIcon: 
*libreoffice.wsIcon: 
LBRY*wsIcon: 
Ulauncher*wsIcon:
```

### i3 config
>Uses the number:name mode for renaming workspaces, so specifying shortcuts this way will keep them working even if they are renamed

```
# Workspaces
set_from_resource $ws0 i3wm.ws0.name "0: "
set_from_resource $ws1 i3wm.ws1.name "1:"
set_from_resource $ws2 i3wm.ws2.name "2:"
set_from_resource $ws3 i3wm.ws3.name "3:"
set_from_resource $ws4 i3wm.ws4.name "4:"
set_from_resource $ws5 i3wm.ws5.name "5:"
set_from_resource $ws6 i3wm.ws6.name "6:"
set_from_resource $ws7 i3wm.ws7.name "7:"
set_from_resource $ws8 i3wm.ws8.name "8:"
set_from_resource $ws9 i3wm.ws9.name "9: "

# Start i3-autoname
exec --no-startup-id i3-autoname

# switch to workspace
bindsym $mod+0 workspace number 0
bindsym $mod+1 workspace number 1
bindsym $mod+2 workspace number 2
bindsym $mod+3 workspace number 3
bindsym $mod+4 workspace number 4
bindsym $mod+5 workspace number 5
bindsym $mod+6 workspace number 6
bindsym $mod+7 workspace number 7
bindsym $mod+8 workspace number 8
bindsym $mod+9 workspace number 9

# move focused container to workspace
bindsym $mod+Shift+0 move container to workspace number 0
bindsym $mod+Shift+1 move container to workspace number 1
bindsym $mod+Shift+2 move container to workspace number 2
bindsym $mod+Shift+3 move container to workspace number 3
bindsym $mod+Shift+4 move container to workspace number 4
bindsym $mod+Shift+5 move container to workspace number 5
bindsym $mod+Shift+6 move container to workspace number 6
bindsym $mod+Shift+7 move container to workspace number 7
bindsym $mod+Shift+8 move container to workspace number 8
bindsym $mod+Shift+9 move container to workspace number 9
```

### Polybar config (optional)
>Don't forget to set up some fonts that support your icons!

```
[base/bar]
  font-0 = DejaVuSans:regular:pixelsize=12;4
  font-1 = Font Awesome 5 Free Regular:pixelsize=14;4
  font-2 = Font Awesome 5 Free Solid:pixelsize=14;4
  font-3 = Font Awesome 5 Brands:pixelsize=14;4
  font-4 = DejaVuSansMono Nerd Font:mono:pixelsize=20;6

[bar/primary]
  inherit        = base/bar
  modules-center = i3

[module/i3]
  type            = internal/i3
  pin-workspaces  = true
  strip-wsnumbers = true

  label-mode      = %mode%
  label-focused   = %index% %name%
  label-unfocused = %index% %name%
  label-visible   = %index% %name%
  label-urgent    = %index% %name%
```

License
-------
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>

[i3wm]: https://i3wm.org/
[Bash]: https://www.gnu.org/software/bash/
[jq]: https://stedolan.github.io/jq/
[Polybar]: https://polybar.github.io/
[i3 module]: https://github.com/polybar/polybar/wiki/Module:-i3
[Font Awesome]: https://fontawesome.com/
[Nerd Fonts]: https://www.nerdfonts.com/
